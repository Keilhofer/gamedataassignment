﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Groundhog : MonoBehaviour
{
    private Vector2 startPosition;
    public float speed;
    public int direction;

    public float growthSpeed;
    public int growthDirection;

    public float timer;
    public float timeToMove = 5;

    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        RunTimer();
        MoveGroundhog();
        GrowGroundhog();
    }

    void RunTimer()
    {
        timer += Time.deltaTime;
    }

    void MoveGroundhog()
    {
        transform.Translate(Vector3.right * speed * direction * Time.deltaTime);

        if (timer >= timeToMove)
        {
            direction = direction * -1;
            timer = 0;
        }
    }

    void GrowGroundhog()
    {
        transform.localScale += new Vector3(growthSpeed * direction * Time.deltaTime, growthSpeed * direction * Time.deltaTime, 1);

        if (timer >= timeToMove)
        {
            direction = direction * -1;
            timer = 0;
        }
    }
}
