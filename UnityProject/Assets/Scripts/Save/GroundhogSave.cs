﻿using System;
using UnityEngine;

[Serializable]
public class GroundhogSave : Save
{
    public Data data;
    private Groundhog groundhog;
    private string jsonString;

    //Data variables to be saved and loaded
    [Serializable]
    public class Data : BaseData
    {
        public Vector3 position;
        public Vector3 scale;
        public float timer;
        public int direction;
        public int growthDirection;
    }

    void Awake()
    {
        groundhog = GetComponent<Groundhog>();
        data = new Data();
    }

    public override string Serialize()
    {
        data.prefabName = prefabName;
        data.position = groundhog.transform.position;
        data.scale = groundhog.transform.localScale;
        data.timer = groundhog.timer;
        data.direction = groundhog.direction;
        data.growthDirection = groundhog.growthDirection;

        jsonString = JsonUtility.ToJson(data);
        return (jsonString);
    }
    //Takes the data and uses it to overwrite the current variables
    public override void Deserialize(string jsonData)
    {
        JsonUtility.FromJsonOverwrite(jsonData, data);
        groundhog.transform.position = data.position;
        groundhog.transform.localScale = data.scale;
        groundhog.timer = data.timer;
        groundhog.direction = data.direction;
        groundhog.growthDirection = data.growthDirection;
        groundhog.transform.parent = GameObject.Find("Groundhogs").transform;
    }
}
